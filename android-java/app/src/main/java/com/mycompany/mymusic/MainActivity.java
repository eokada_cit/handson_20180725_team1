package com.mycompany.mymusic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.mymusic.commons.SearchTextWatcher;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    TextView warning;

    EditText search;

    ImageView action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        action = (ImageView) findViewById(R.id.bt_search);
        search = (EditText) findViewById(R.id.ed_search);
        warning = (TextView) findViewById(R.id.tv_warning_info);
        action.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SearchTextWatcher textWatcher = new SearchTextWatcher(search, search, warning, action);
        search.addTextChangedListener(textWatcher);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_search:
                Toast.makeText(this, "Teste", Toast.LENGTH_LONG).show();
                QueryResultActivity.startActivity(this,null);
                // api call
                return;
            default:
        }
    }
}
