package com.mycompany.mymusic.music.service;

import com.mycompany.mymusic.music.json.Music;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MusicService {

    @GET("/api/musicas")
    Single<Music> getMusic(@Query("filtro") String filtro);

}
