package com.mycompany.mymusic.commons;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mycompany.mymusic.R;

public class SearchTextWatcher implements TextWatcher {

    private EditText search;
    private View view;
    private TextView warning;
    private ImageView action;

    public SearchTextWatcher(EditText search, View view, TextView warning, ImageView action) {
        this.search = search;
        this.view = view;
        this.warning = warning;
        this.action = action;
    }

    @Override
    public void beforeTextChanged(CharSequence sequence, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence sequence, int start, int before, int count) {
        if (sequence.length() < 1) {
            warning.setText(view.getContext().getString(R.string.warning_empty_or_lenght));
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() < 1) {
            warning.setText(view.getContext().getString(R.string.warning_empty_or_lenght));
        }
    }
}
