package com.mycompany.mymusic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mycompany.mymusic.music.json.Music;

public class QueryResultActivity extends AppCompatActivity {

    public static String MUSIC = "com.mycompany.mymusic.QueryResultActivity.MUSIC";

    private Music music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static void startActivity(Context context, Music music) {
        Intent intent = new Intent(context, QueryResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(QueryResultActivity.MUSIC, music);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

}
