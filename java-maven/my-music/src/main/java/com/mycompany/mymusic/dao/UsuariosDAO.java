package com.mycompany.mymusic.dao;

import com.mycompany.mymusic.entities.Usuarios;
import org.springframework.data.repository.CrudRepository;

public interface UsuariosDAO extends CrudRepository<Usuarios, String> {

  Usuarios findUsuariosByNome(String nome);



}