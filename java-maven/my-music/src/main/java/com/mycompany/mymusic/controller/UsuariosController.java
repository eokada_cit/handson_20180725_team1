package com.mycompany.mymusic.controller;

import com.mycompany.mymusic.dao.PlaylistDAO;
import com.mycompany.mymusic.dao.UsuariosDAO;
import com.mycompany.mymusic.entities.Playlist;
import com.mycompany.mymusic.entities.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuariosController {

  @Autowired
  private UsuariosDAO usuariosDAO;
  @Autowired
  private PlaylistDAO playlistDAO;

  @RequestMapping("/api/playlists")
  public Playlist mostraUsuarios(String user) {

    Usuarios usuarios = usuariosDAO.findUsuariosByNome(user);

    System.out.println(playlistDAO.findPlaylistMusicaByPlaylistId(usuarios.getId()));

    return null;
  }

}