package com.mycompany.mymusic.entities;

public class PlaylistItem {

  private String musica;
  private String artisa;

  public String getMusica() {
    return musica;
  }

  public void setMusica(String musica) {
    this.musica = musica;
  }

  public String getArtisa() {
    return artisa;
  }

  public void setArtisa(String artisa) {
    this.artisa = artisa;
  }
}
