package com.mycompany.mymusic.entities;

import java.util.List;

public class Playlist {

  private List<PlaylistItem> playlistItemList;

  public List<PlaylistItem> getPlaylistItemList() {
    return playlistItemList;
  }

  public void setPlaylistItemList(List<PlaylistItem> playlistItemList) {
    this.playlistItemList = playlistItemList;
  }
}
