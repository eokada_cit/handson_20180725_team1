package com.mycompany.mymusic.dao;

import com.mycompany.mymusic.entities.PlaylistMusica;
import com.mycompany.mymusic.entities.Usuarios;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaylistDAO extends CrudRepository<Usuarios, String> {

  List<PlaylistMusica> findPlaylistMusicaByPlaylistId(String id);
}
