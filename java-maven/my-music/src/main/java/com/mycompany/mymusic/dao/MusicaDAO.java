package com.mycompany.mymusic.dao;

import com.mycompany.mymusic.entities.Musica;
import com.mycompany.mymusic.entities.Usuarios;
import org.springframework.data.repository.CrudRepository;

public interface MusicaDAO extends CrudRepository<Usuarios, String> {

  Musica findMusicaById(String id);
}
