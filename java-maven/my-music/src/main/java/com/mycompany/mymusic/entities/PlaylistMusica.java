package com.mycompany.mymusic.entities;

import javax.persistence.Column;

public class PlaylistMusica {

  @Column(name = "playlistid")
  private String playlistId;
  @Column(name = "musicaid")
  private String musicaId;

  public String getPlaylistId() {
    return playlistId;
  }

  public void setPlaylistId(String playlistId) {
    this.playlistId = playlistId;
  }
}
